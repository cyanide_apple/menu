/****************************
*Menu.h                     *
*****************************
*author : Tugce Dinc        *
*IDE : Visual Studio 2015   *
*Last Update: 29/12/2018    *
*****************************/
#ifndef Menu_h
#define Menu_h
#include<iostream>
#include"PioneerRobotAPI.h"
#include "RobotControl.h"
#include "SonarSensor.h"
#include "LaserSensor.h"
#include "Path.h"
#include "Record.h"
#include "Pose.h"
#include "RobotOperator.h"
#include "PioneerRobotInterface.h"
#include "RobotInterface.h"
#include "RangeSensor.h"
using namespace std;
/**
  *\author TUGCE DINC
  *\brief This class operates the menus.
  */
class Menu
{
	/**
	  *\brief It contains Robot information.
	  */
	PioneerRobotAPI *robot;
	/**
	  *\brief It includes some functions to control the Robot(it uses an interface).
	  */
	RobotControl *robotCont;
	/**
	  *\brief It includes some functions to control the Robot.
	  */
	PioneerRobotInterface* pioneerRobotInterface;
	/**
	  *\brief It includes some functions to control the Robot.
	  */
	RobotOperator *robotoperator;
	/**
	  *\brief It used for eyncription.
	  */
	LaserSensor *lasesen;
	/**
	  *\brief It operates the sonar sensors of the Robot.
	  */
	SonarSensor *sonarsen;
	/**
	  *\brief It controls if the robot is connected.
	  */
	bool connection = false;
public:
	/**
	  *\brief constructor
	  */
	Menu(PioneerRobotAPI *robot, PioneerRobotInterface*);
	/**
	  *\brief It operates The Main Menu.
	  */
	void MainMenu();
	/**
	  *\brief It operates The Connection Menu.
	  */
	void ConnectionMenu();
	/**
	  *\brief It operates The Motion Menu.
	  */
	void MotionMenu();
	/**
	  *\brief It operates The Sensor Menu.
	  */
	void SensorMenu();

	/**
	  *\brief It lists information about Sonar Sensor.
	  */
	void SonarSensorInfo();
	/**
	  *\brief It lists information about Laser Sensor.
	  */	
	void LazerSensorInfo();
	/**
	  *\brief Robot is Connected in this function.
	  */
	void ConnectionRobot();
	/**
	  *\brief Robot is Disconnected in this function.
	  */
	void DisconnectionRobot();
	/**
	  *\brief Robot is turned left in this function.
	  */
	void TurnLeftRobot();
	/**
	  *\brief Robot is turned right in this function.
	  */
	void TurnRightRobot();
	/**
	  *\brief Robot is moved Forward with a speed in this function.
	  */
	void Forward();
	/**
	  *\brief The path is removed. 
	  */
	void ClearPath();
	/**
	  *\brief The path is recorded in a file.
	  */
	void RecordPath();
	/**
	  *\brief The path is listed in this function.
	  */
	void Listpath();
	/**
	  *\brief Robot is moved safely in this function.
	  */
	void safeMove();
	/**
	  *\brief Robot is moved bacward with a speed in this function.
	  */
	void backward();

	/**
	*\brief Robot is moved in this function.
	*/
	void Move();
	/**
	  *\brief Destructor.
	  */
	~Menu();
};

#endif // !Menu_h