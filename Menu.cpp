/****************************
*Menu.cpp                   *
*****************************
*author : Tugce Dinc        *
*IDE : Visual Studio 2015   *
*Last Update: 29/12/2018    *
*****************************/

#include "Menu.h"

Menu::Menu(PioneerRobotAPI *robot, PioneerRobotInterface *interfacee)
{
	this->pioneerRobotInterface = interfacee;
	this->robot = robot;
	this->robotoperator = new RobotOperator(1234);
	this->robotCont = new RobotControl(pioneerRobotInterface, robotoperator);
	this->lasesen = new LaserSensor(robot);
	this->sonarsen = new SonarSensor(robot);
}
void Menu::MainMenu()
{
	cout << "Please enter your pasword  >>  ";
	int a;
	cin >> a;
	if (true == robotCont->openAccess(a))
	{
		while (1)
		{
			int choice1;
			int choise2;
			cout << "Main Menu\n"
				<< "1.  Connection\n"
				<< "2.  Motion\n"
				<< "3.  Sensor\n"
				<< "4.  Quit\n"
				<< "Choose one : ";
			cin >> choice1;
			switch (choice1)
			{
			case 1:
				ConnectionMenu();
				break;
			case 2:
				MotionMenu();
				break;
			case 3:
				SensorMenu();
				break;
			case 4:

				cout << "Please enter your pasword  >>  ";
				cin >> a;
				if (true == robotCont->openAccess(a))
				{
					return;
				}
			default:
				cout << "Error!" << endl;
				break;
			}
		}
	}
	else
	{
		cout << "Error!!!" << endl;
	}
}

void Menu::ConnectionMenu()
{
	while (1)
	{
		int choise2;
		cout << "Connection Menu\n"
			<< "1.  Connection Robot\n"
			<< "2.  DisConnect Robot\n"
			<< "3.  Back\n"
			<< "Choose one : ";
		cin >> choise2;
		switch (choise2)
		{
		case 1:
			ConnectionRobot();
			break;
		case 2:
			DisconnectionRobot();
			break;
		case 3:
			return;
		default:
			cout << "Error!" << endl;
			break;
		}
	}
}

void Menu::ConnectionRobot()
{
	if (connection==true) {
		cout << "Could not connect..." << endl;
		return;
	}
	else
	{
		pioneerRobotInterface->connect();
		cout << "Robot is connected.\n";
		connection = true;
		return;
	}
}

void Menu::DisconnectionRobot()
{
	if (connection == true)
	{
		pioneerRobotInterface->disconnect();
		cout << "Robot is disconnected" << endl;
		connection = false;
	}
	else
	{
		cout << "Error\n";
	}
}

//--------------------------------------------------------
void Menu::MotionMenu()
{
	while (1)
	{
		int choise2;
		cout << "Motion Menu\n"
			<< "1.  Move Robot\n"
			<< "2.  Safe Move Robot\n"
			<< "3.  Turn Left\n"
			<< "4.  Turn Right\n"
			<< "5.  Forward\n"
			<< "6.  Move Distance\n"
			<< "7.  Clear Path\n"
			<< "8.  Record Path\n"
			<< "9.  List Path(which is recorted)\n"
			<< "10. Backward\n"
			<< "11. Back\n"
			<< "Choose one : ";
		cin >> choise2;
		switch (choise2)
		{
		case 1:
			Move();
			break;
		case 2:
			safeMove();
			break;
		case 3:
			TurnLeftRobot();
			break;
		case 4:
			TurnRightRobot();
			break;
		case 5:
			Forward();
			break;
		case 6:
			Forward();
			break;
		case 7:
			ClearPath();			
			break;
		case 8:
			RecordPath();
			break;
		case 9:
			Listpath();
			break;
		case 10:
			backward();
			break;
		case 11:
			return;
		default:
			cout << "Error!" << endl;
			break;
		}

	}
}
void Menu::TurnLeftRobot()
{
	if (connection == true)
	{
		Pose pose;
		pose.setX(robot->getX());
		pose.setY(robot->getY());
		pose.setTh(robot->getTh());
		robotCont->setPose(&pose);

		robotCont->addToPath();
		robotCont->turnLeft();
		robotCont->stopTurn();
		cout << "The robot turned left" << endl;
	}
	else
	{
		cout << "Robot disconnected" << endl;
	}
}
void Menu::TurnRightRobot()
{
	if (connection == true)
	{
		Pose pose;
		pose.setX(robot->getX());
		pose.setY(robot->getY());
		pose.setTh(robot->getTh());
		robotCont->setPose(&pose);

		robotCont->addToPath();
		robotCont->turnRight();
		robotCont->stopTurn();
		cout << "The robot turned right" << endl;
	}
	else
	{
		cout << "Robot disconnected" << endl;
	}
}
void Menu::Forward()
{
	if (connection == true)
	{
		int speed;
		cout << "Please, enter the speed  >>  ";
		cin >> speed;
		
		Pose pose;
		pose.setX(robot->getX());
		pose.setY(robot->getY());
		pose.setTh(robot->getTh());
		robotCont->setPose(&pose);
		
		robotCont->addToPath();
		robotCont->forward(speed);
		robotCont->stopMove();
		cout << "The robot is moved" << endl;
	}
	else
	{
		cout << "Robot disconnected" << endl;
	}
}

void Menu::ClearPath()
{
	if (connection == true)
	{
		ofstream file("record.txt");
		file.close();
		robotCont->clearPath();
		cout << "The path is cleared" << endl;
	}
	else
	{
		cout << "Robot disconnected" << endl;
	}
}

void Menu::RecordPath()
{
	if (connection == true)
	{
		robotCont->recordPathToFile();
		cout << "The path is recorded" << endl;
	}
	else
	{
		cout << "Robot disconnected" << endl;
	}
}

void Menu::Listpath()
{
	if (connection == true)
	{
		Record output;
		output.setFileName("record.txt");
		string str;
		int i = 0;
		if (output.openFile() == true)
		{
			cout << "Path List" << endl;

			while (1)
			{
				output >> str;
				if (str != "")
				{
					cout << ++i << ". " << str << endl;
				}
				else
				{
					break;
				}
				str = "";
			}
		}
	}
	else
	{
		cout << "Robot disconnected" << endl;
	}
}

void Menu::safeMove()
{
	int i;
	while (1)
	{
		robot->moveRobot(300);
		if (sonarsen->getMin(i) <= 750)
		{
			robotCont->stopMove();
			break;
		}
	}
}

void Menu::backward()
{
	if (connection == true)
	{
		Pose pose;
		pose.setX(robot->getX());
		pose.setY(robot->getY());
		pose.setTh(robot->getTh());
		robotCont->setPose(&pose);

		robotCont->addToPath();
		robotCont->forward(-300);
		robotCont->stopMove();
		cout << "The robot is moved" << endl;
	}
	else
	{
		cout << "Robot disconnected" << endl;
	}
}

void Menu::Move() {
	if (connection == true)
	{
		Pose pose;
		pose.setX(robot->getX());
		pose.setY(robot->getY());
		pose.setTh(robot->getTh());
		robotCont->setPose(&pose);

		robotCont->addToPath();
		robotCont->forward(500);
		robotCont->stopMove();
		cout << "The robot is moved" << endl;
	}
	else
	{
		cout << "Robot disconnected" << endl;
	}
}
void Menu::SensorMenu()
{
	while (1)
	{
		int choise2;
		cout << "Sensor Menu\n"
			<< "1.  Sonar Sensor Info\n"
			<< "2.  Laser Sensor Info\n"
			<< "3.  Quit\n"
			<< "Choose one : ";

		cin >> choise2;
		switch (choise2)
		{
		case 1:
			SonarSensorInfo();
			break;
		case 2:
			LazerSensorInfo();
			break;
		case 3:
			return;
		default:
			cout << "Error!" << endl;
			break;
		}
	}
}

void Menu::SonarSensorInfo()
{
	if (connection == true)
	{
		//sonarsen = new SonarSensor(robot);
		cout << "Sonar Sensors' ranges  = [ ";
		for (int i = 0; i < 16; i++)
		{
			cout << sonarsen->getRange(i);
			if (i != 15)
			{
				cout << ", ";
			}
		}
		cout << " ]" << endl;
		int min, max;
		int valuemax = sonarsen->getMax(max);
		int valuemin = sonarsen->getMin(min);
		cout << "Mininum range = range[" << min << "] = " << valuemin << endl;
		cout << "Maximum range = range[" << max << "] = " << valuemax << endl;
	}
	else
	{
		cout << "Robot disconnected" << endl;
	}
}

void Menu::LazerSensorInfo()
{
	if (connection == true)
	{
		cout << "laser Sensors' ranges  = [ ";
		for (int i = 0; i < 181; i++)
		{
			cout << lasesen->getRange(i);
			if (i != 180)
			{
				cout << ", ";
			}
		}
		cout << " ]" << endl;
		int min, max;
		int valuemax = lasesen->getMax(max);
		int valuemin = lasesen->getMin(min);
		cout << "Mininum range = range[" << min << "] = " << valuemin << endl;
		cout << "Maximum range = range[" << max << "] = " << valuemax << endl;
	}
	else
	{
		cout << "Robot disconnected" << endl;
	}
}
Menu::~Menu()
{
}
