#include<iostream>
#include "Menu.h"
#include"PioneerRobotAPI.h"

using namespace std;

int main(void) {

	PioneerRobotAPI *robot = new PioneerRobotAPI;
	SonarSensor *sonarsensor = new SonarSensor(robot);
	PioneerRobotInterface *	pioneerRobotInterface = new PioneerRobotInterface(robot, sonarsensor);
	Menu menu(robot, pioneerRobotInterface);
	menu.MainMenu();

	system("pause");
	return 0;
}